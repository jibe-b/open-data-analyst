---
title: A platform where to find data to analyse or open your data to find an analyst
...



Eager to try?

[Data analyst: access the analysis platform](analysis/)

[Data owner: follow the data opening helper](helper/)


## Description


The first goal of the team behind the platform is to increase the opening of data in 5\* quality and increase the adoption of analysis of open data.

## Open Data

Tons of data are produced, in particular in science. Longitudinal analyses and bid data analyses can only be applied if the data can be accessed by the analysts.

## Analyses

The skills of data scientists are only one download away from you and your team. This platform is here to make you realize what may be done with your data and harvest the results: the resulting data will be open so you may use it too!



## 5*

90% of the work in the full pipeline of data management is data cleaning. All data on the platform must follow 5\*criteria. You will find help to get your data to this level of quality so that it be quickly used.

## Open

Any data produced making use of this platform will be released under an open license. This way, the resulting data becomes open data.


## Contribution

This platform is in alpha state.

The main roadmap is defined, yet tons of details are still to be decided and implemented. This project will be developed in connection with the community. 

Come join the adventure, on [gitlab](https://gitlab.com/jibe-b/open-data-analyst) and on [twitter](https://twitter.com/jibe_jeybee)!

