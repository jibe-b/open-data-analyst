# Open data analyst

## Description

This project aims to:

- free datasets
- ease the cooperation between data owners and data analysts
- ease and track the production of results through the opening of datasets

The paltform is a tool used to achieved this goal and all data and software are aimed to be integrated at middle-term in the decentralized web.

## Contributing

Please open issues to submit feature requests, bugs or any question.

Pull requests are also welcome, even though this project is still in an alpha stage.

## Skills

The main developer of this project (@jibe-b) of this project is still under acquisition of the skills of software craftsmanship. I am looking for skilled programmers to mentor me by reviewing the code I  write. Regular comments with suggestions would be highly welcome.
