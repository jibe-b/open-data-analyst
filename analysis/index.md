---
title: A platform where to find open datasets and execute analyses
...

## Intro

## Open Data search engine: find a dataset

<iframe src='https://searx.laquadrature.net/?categories=science' width='100%' height='50%'></iframe>

Coming soon. Teaser: this search engine will reference datasets that are available under an open license, with categories to help choosing the right dataset

## Execute an analysis

1. build your scripts
  - never learned programming? [Program visually](../galaxy-workflow-editor.md)
  - upload your script

2. execute the analysis

[Run the workflow](../workflow/run?id=1)

## Cooperate around datasets, analysis workflows and results!

See the [graph of datasets](datasets-graph/)

Examine available [workflows](workflows/)

Space to collaborate on [results](results/)
